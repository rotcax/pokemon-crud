export class CreatePokemonDto {
  name: string;
  type: string;
  generation: number;
  imageUrl: string;
}
