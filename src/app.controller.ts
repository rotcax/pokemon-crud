import { Controller, Get, Post, Put, Delete, Param, Body, Res, HttpCode } from '@nestjs/common';
import { Response } from 'express';
import { AppService } from './app.service';
import { CreatePokemonDto } from './dto';
import { response } from './utils';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService
  ) { }

  @Get()
  async getAll(@Res() res: Response) {
    const pokemons = await this.appService.getAll();
    return res.json(response(pokemons));
  }

  @Get(':id')
  async get(@Res() res: Response, @Param('id') id: string) {
    const pokemon = await this.appService.get(id);
    return res.json(response(pokemon));
  }

  @Post()
  @HttpCode(201)
  async save(@Res() res: Response, @Body() createPokemonDto: CreatePokemonDto) {
    try {
      const pokemon = await this.appService.save(createPokemonDto);
      return res.json(response(pokemon));

    } catch(err) {
      return res.status(400).json({ 'result': 'Field validation failed' });
    }
  }

  @Put(':id')
  async update(@Res() res: Response, @Param('id') id: string, @Body() createPokemonDto: CreatePokemonDto) {
    try {
      const pokemon = await this.appService.update(id, createPokemonDto);
      return res.json(response(pokemon));

    } catch(err) {
      return res.status(400).json({ 'result': 'Field validation failed' });
    }
  }

  @Delete(':id')
  @HttpCode(204)
  async delete(@Res() res: Response, @Param('id') id: string) {
    this.appService.delete(id);
    return res.json(response(''));
  }
}
