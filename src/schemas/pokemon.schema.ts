import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PokemonDocument = Pokemon & Document;

@Schema()
export class Pokemon {
  @Prop({ required: true })
  name: string

  @Prop({ required: true })
  type: string

  @Prop({ required: true })
  generation: string

  @Prop({ required: true })
  imageUrl: string
}

export const PokemonSchema = SchemaFactory.createForClass(Pokemon)
