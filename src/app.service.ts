import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Pokemon, PokemonDocument } from './schemas';
import { CreatePokemonDto } from './dto'

@Injectable()
export class AppService {
  constructor(
    @InjectModel('pokemon') private pokemonModel: Model<PokemonDocument>
  ) { }

  async getAll(): Promise<Pokemon[]> {
    return this.pokemonModel.find().exec();
  }

  async get(id: string): Promise<Pokemon> {
    return this.pokemonModel.findById(id).exec();
  }

  async save(createPokemonDto: CreatePokemonDto): Promise<Pokemon> {
    const pokemon = new this.pokemonModel(createPokemonDto);
    return pokemon.save();
  }

  async update(id: string, createPokemonDto): Promise<Pokemon> {
    const pokemon = this.pokemonModel.findByIdAndUpdate(id, createPokemonDto, { new: true }).exec();
    return pokemon;
  }

  async delete(id: string) {
    return this.pokemonModel.findByIdAndDelete(id).exec();
  }
}
