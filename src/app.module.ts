import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PokemonSchema } from './schemas';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/pokemonCrud', {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }),
    MongooseModule.forFeature([{ name: 'pokemon', schema: PokemonSchema }])
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
